const path = require("path");

module.exports = {
  lintOnSave: false,

  pluginOptions: {
    'style-resources-loader': {
      preProcessor: 'scss',
      patterns: [path.resolve(__dirname, "./src/assets/styles/main.scss")],
    },
  },

  pwa: {
    themeColor: '#261a42',
    msTileColor: '#261a42'
  },
};
