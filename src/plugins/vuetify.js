import Vue from 'vue';
import Vuetify from 'vuetify/lib';
import 'vuetify/src/stylus/app.styl';

Vue.use(Vuetify, {
  iconfont: 'md',
  theme: {
    primary: '#261a42',
    grey: '#929497',
    accent: 'ec008c',
    error: '#b71c1c',
  },
});
