module.exports = {
  module: {
    rules: [
      {
        test: /\.scss$/,
        use: {
          loader: 'sass-loader',
          options: {
            includePaths: [
              path.resolve('../node_modules'),
            ]
          }
        }
      }
    ]
  }
}